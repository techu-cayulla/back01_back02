package com.techu.codelabs.back01.controller;

import com.techu.codelabs.back01.model.ProductoModel;
import com.techu.codelabs.back01.model.ProductoPrecioOnlyModel;
import com.techu.codelabs.back01.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;


@RestController
@RequestMapping("/producto")
public class ProductoController {

    private final ProductoService productoService;

    @Autowired
    public ProductoController(ProductoService productoService)
    {
        this.productoService = productoService;
    }

    @GetMapping("/{id}")
    public ResponseEntity getProductoId(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoByID(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(pr);
    }

    @PostMapping()
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel producto)
    {
        productoService.addProducto(producto);
        return new ResponseEntity<>("Product created successfully!", HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateProducto(@PathVariable String id,
                                         @RequestBody ProductoModel productToUpdate) {
        ProductoModel pr = productoService.getProductoByID(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.updateProducto(productToUpdate);
        return new ResponseEntity<>("Producto actualizado correctamente.", HttpStatus.OK); // Buenas prácticas dicen que mejor enviar 204 No Content
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteProducto(@PathVariable String id) {
        ProductoModel pr = productoService.getProductoByID(id);
        if(pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        productoService.deleteProducto(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT); // status code 204-No Content
    }

    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(
            @RequestBody ProductoPrecioOnlyModel productoPrecioOnly, @PathVariable String id){
        ProductoModel pr = productoService.getProductoByID(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        pr.setPrecio(productoPrecioOnly.getPrecio());
        productoService.updateProducto(pr);
        return new ResponseEntity<>(pr, HttpStatus.OK);
    }

    @GetMapping("/{id}/users")
    public ResponseEntity getProductIdUsers(@PathVariable String id){
        ProductoModel pr = productoService.getProductoByID(id);
        if (pr == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (pr.getUsers()!=null)
            return ResponseEntity.ok(pr.getUsers());
        else
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
