package com.techu.codelabs.back01.service;

import com.techu.codelabs.back01.model.ProductoModel;
import com.techu.codelabs.back01.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("productoService")
@Transactional
public class ProductoServiceImpl implements ProductoService{
    @Autowired
    ProductoRepository productoRepository;

    @Override
    public ProductoModel getProductoByID(String id) {
        return productoRepository.getProductoByID(id);
    }

    @Override
    public void addProducto(ProductoModel productoModel) {
        productoRepository.addProducto(productoModel);
    }

    @Override
    public void updateProducto(ProductoModel productoModel) {
        productoRepository.updateProducto(productoModel);
    }

    @Override
    public void deleteProducto(String id) {
        productoRepository.deleteProducto(id);
    }
}
