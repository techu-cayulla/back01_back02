package com.techu.codelabs.back01.service;

import com.techu.codelabs.back01.model.ProductoModel;

public interface ProductoService {

    ProductoModel getProductoByID(String id);
    void addProducto(ProductoModel productoModel);

    public void updateProducto(ProductoModel productoModel);
    public void deleteProducto(String id);
}
