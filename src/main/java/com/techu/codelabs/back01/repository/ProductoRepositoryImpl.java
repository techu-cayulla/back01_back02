package com.techu.codelabs.back01.repository;

import com.techu.codelabs.back01.model.ProductoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProductoRepositoryImpl implements ProductoRepository{

    private final MongoOperations mongoOperations;

    @Autowired
    public ProductoRepositoryImpl(MongoOperations mongoOperations) {
        this.mongoOperations = mongoOperations;
    }


    @Override
    public ProductoModel getProductoByID(String id) {
        ProductoModel encontrado = this.mongoOperations.findOne(new Query(Criteria.where("id").is(id)),ProductoModel.class);
        return encontrado;
    }

    @Override
    public void addProducto(ProductoModel productoModel) {
        this.mongoOperations.save(productoModel);
    }

    @Override
    public void updateProducto(ProductoModel productoModel) {
        this.mongoOperations.save(productoModel);
    }

    @Override
    public void deleteProducto(String id) {
        this.mongoOperations.findAndRemove(new Query(Criteria.where("id").is(id)), ProductoModel.class);
    }

}
