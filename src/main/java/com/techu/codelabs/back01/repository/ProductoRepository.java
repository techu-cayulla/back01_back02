package com.techu.codelabs.back01.repository;

import com.techu.codelabs.back01.model.ProductoModel;

import java.util.List;

public interface ProductoRepository {

    ProductoModel getProductoByID(String id);
    void addProducto(ProductoModel productoModel);

    public void updateProducto(ProductoModel productoModel);
    public void deleteProducto(String id);
}
