package com.techu.codelabs.back01.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "producto")
public class ProductoModel {

    @Id
    @NotNull
    private String id;
    @NotNull
    private String nombre;
    private double precio;
    private List<UserModel> users;

    public ProductoModel(String id, String nombre, double precio, List<UserModel> users){
        setNombre(nombre);
        setId(id);
        setPrecio(precio);
        setUsers(users);
    }
//    public ProductoModel(String id, String nombre, double precio){
//        setNombre(nombre);
//        setId(id);
//        setPrecio(precio);
//    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void setUsers(List<UserModel> users) {
        this.users = users;
    }
}
