package com.techu.codelabs.back01.model;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "users")
public class UserModel {

    private String user;
    private String name;

    public UserModel(String user, String name){
        this.setName(name);
        this.setUser(user);
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
