package com.techu.codelabs.back01.model;

public class ProductoPrecioOnlyModel {
    private long id;
    private double precio;

    public void ProductoPrecioOnly() {
    }

    public void ProductoPrecioOnly(double precio) {
        this.precio = precio;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
